<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="un bestiaire animal pour la region des hyliens">
    <link rel="icon" href="https://img2.freepng.fr/20180203/epq/kisspng-favicon-clip-art-buggi-5a7595dde77908.0364596915176555179481.jpg">
    <?php bestiaire_register_assets()?> 
    <title>bestiaire</title>
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri();?>">
    <?php wp_head() ?>
</head>
<body>
<header>
      <h1>Le Bestiaire</h1>
</header>

