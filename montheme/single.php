<?php get_header() ?>
<div class="grid-single">
    <div class="one">
        <h1><?php the_title() ?></h1>
        <p><?php the_content() ?></p>
    </div>
    <div class="two">
        <div class="img"><?php the_post_thumbnail('medium') ?></div>
        
        <?php 
        $lien = get_field('lien');
        if( $lien ):?>
        <p>mange :</p>
        <div class="mange">
        <?php 
        foreach($lien as $post):
        setup_postdata($post);
        ?>
        
        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

        <?php 
        endforeach; 
        wp_reset_postdata();
        endif;
        ?>
        <p><?php the_field('taille')?> toise</p>
        </div>
    </div>    
</div>
<?php get_footer() ?>